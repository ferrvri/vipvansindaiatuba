<?php

require_once('class.phpmailer.php');
require_once('./connection.php');

$result = false;

if ($con && isset($_POST['nome']) && isset($_POST['email']) && isset($_POST['detalhes'])) {
    $stmt = $con->prepare("INSERT INTO contatos (cont_nome, cont_email, cont_telefone, cont_detalhes) values (?, ?, ?, ?) ");
    $stmt->bind_param('ssss', $_POST['nome'], $_POST['email'], $_POST['telefone'], $_POST['detalhes']);

    if ($stmt->execute()) {
        $assunto  = 'Novo Contato [SITE]: ' . $_POST['nome'];
        $mensagem = '<html>' .
            '<meta charset="utf-8">' .
            '<body>' .
            '<div style="display: table; margin: auto; width: 100%">' .
            '<div style="width: 500px; border: 1px solid #dcdcdc; text-align: center">' .
            '<div style="width: 100%; border-bottom: 1px solid #dcdcdc; height: 100px; background: #282828">' .
            '<img src="https://vipvansindaiatuba.com.br/images/logo.png" style="width: 35%; margin: auto; display: table;"/>' .
            '</div>' .
            '<span style="position: relative; top: 15px"> Você possui um novo contato! </span>' .

            '<div style="display: table; margin: 10px auto; margin-bottom: 25px">' .
            '<p style="font-weight: bold">Nome: ' .$_POST['nome']. '</p>'.
            '<p style="font-weight: bold">E-mail: ' .$_POST['email']. '</p>'.
            '<p style="font-weight: bold">Telefone: ' .$_POST['telefone']. '</p>'.
            '<p style="font-weight: bold">Detalhes: </p>' .
            '<span>' .
            $_POST['detalhes'] .
            '</span>' .
            '</div>' .

            '<div style="width: 100%; background: #272727">' .
            '<p style="color: #fff; padding: 4px "> Enviado por: <a href="https://vipvansindaiatuba.com.br/">https://vipvansindaiatuba.com.br/</a> </p>' .
            '</div>' .
            '</div>' .
            '</div>' .
            '</body>' .
            '</html> ';

        $mail = new PHPMailer(true);
        $mail->IsSMTP();                // Ativar SMTP
        try {
            $mail->CharSet = "UTF-8";
            $mail->Host      = 'mail.vipvansindaiatuba.com.br'; // SMTP utilizado
            $mail->SMTPAuth  = true;  // Usar autenticação SMTP (obrigatório para smtp.seudomínio.com.br)
            $mail->Port      = 25; //  Usar 587 porta SMTP
            $mail->Username  = 'naoresponder@vipvansindaiatuba.com.br';   // SMTP username
            $mail->Password  = '123@Vipvansindaiatuba';     // SMTP password
            // $mail->SMTPSecure = 'tls';   

            //Define o remetente
            // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            $mail->SetFrom('naoresponder@vipvansindaiatuba.com.br', 'VIP Vans Indaiatuba'); //Seu e-mail
            $mail->AddReplyTo('naoresponder@vipvansindaiatuba.com.br', 'VIP Vans Indaiatuba'); //Seu e-mail
            $mail->Subject = $assunto; //Assunto do e-mail

            //Define os destinatário(s)
            //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
            $mail->AddAddress('contato@vipvansindaiatuba.com.br', 'contato');

            //Define o corpo do email
            $mail->MsgHTML($mensagem);

            ////Caso queira colocar o conteudo de um arquivo utilize o método abaixo ao invés da mensagem no corpo do e-mail.
            //$mail->MsgHTML(file_get_contents('arquivo.html'));

            if ($mail->Send()) {
                echo json_encode(array('status' => '0x104'));
            } else {
                echo json_encode(array('status' => '0x102'));
            }
        } catch (phpmailerException $e) {
            echo json_encode(array('status' => '0x101', 'error' => $e->errorMessage()));
        }
    }
}
